package authjwt

import (
    "net/http"
    "time"
    "os"
    "fmt"
    "log"
    "strconv"

    "github.com/dgrijalva/jwt-go"
    "github.com/gin-gonic/gin"
    "github.com/twinj/uuid"
)

func CreateTokens(userid uint64) (*TokenDetails, error) {
    td := &TokenDetails{}
    var err error

    td.AtExpires = time.Now().Add(time.Hour * 24 * 7).Unix()
    td.AccessUuid = uuid.NewV4().String()
 
    td.RtExpires = time.Now().Add(time.Hour * 24 * 7).Unix()
    td.RefreshUuid = uuid.NewV4().String()
 
    atClaims := jwt.MapClaims{}
    atClaims["authorized"] = true
    atClaims["access_uuid"] = td.AccessUuid
    atClaims["user_id"] = userid
    atClaims["exp"] = td.AtExpires
    at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
    td.AccessToken, err = at.SignedString([]byte(os.Getenv("REDIS_ACCESS_SECRET")))
    if err != nil {
        return nil, err
    }

    rtClaims := jwt.MapClaims{}
    rtClaims["refresh_uuid"] = td.RefreshUuid
    rtClaims["user_id"] = userid
    rtClaims["exp"] = td.RtExpires
    rt := jwt.NewWithClaims(jwt.SigningMethodHS256, rtClaims)
    td.RefreshToken, err = rt.SignedString([]byte(os.Getenv("REDIS_REFRESH_SECRET")))
    if err != nil {
        return nil, err
    }
    return td, nil
}

func RefreshTokens(c *gin.Context) {
    refreshToken := ExtractRefreshToken(c.Request)
 
    token, err := jwt.Parse(refreshToken, func(token *jwt.Token) (interface{}, error) {
        //as "SigningMethodHMAC" ??
        if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
            return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
        }
        return []byte(os.Getenv("REDIS_REFRESH_SECRET")), nil
    })

    //Refresh token expired ? 
    if err != nil {
    	log.Println("Refresh token expired ? or another error ??")
        c.JSON(http.StatusUnauthorized, "Refresh token expired")
        return
    }
    //is token valid?
    if _, ok := token.Claims.(jwt.Claims); !ok && !token.Valid {
        c.JSON(http.StatusUnauthorized, err)
        return
    }
    //With valid Token => get the uuid ?:
    claims, ok := token.Claims.(jwt.MapClaims) //the token claims AS MapClaims :)
    if ok && token.Valid {
        refreshUuid, ok := claims["refresh_uuid"].(string)
        if !ok {
            log.Println("Error on claims['refresh_uuid'].(string)")
            c.JSON(http.StatusUnprocessableEntity, err)
            return
        }

        userId, err := strconv.ParseUint(fmt.Sprintf("%.f", claims["user_id"]), 10, 64)
        if err != nil {
            c.JSON(http.StatusUnprocessableEntity, "Error occurred")
            return
        }
        //Delete the previous Refresh Token
        deleted, delErr := DeleteAuth(refreshUuid)
        if delErr != nil || deleted == 0 { //if any goes wrong
            c.JSON(http.StatusUnauthorized, "unauthorized")
            return
        }

        //Delete the previous access token
        au, err := ExtractTokensMetadata(c.Request)
        if err != nil {
            c.JSON(http.StatusUnauthorized, "unauthorized")
            return
        }
        deleted, delErr = DeleteAuth(au.AccessUuid)
        if delErr != nil || deleted == 0 { //if any goes wrong
            c.JSON(http.StatusUnauthorized, "unauthorized")
            return
        }
     
        //Create new pairs of refresh and access tokens
        ts, createErr := CreateTokens(userId)
        if  createErr != nil {
            c.JSON(http.StatusForbidden, createErr.Error())
            return
        }
        
        //save the tokens metadata to redis
        saveErr := CreateAuth(userId, ts)
        if saveErr != nil {
            c.JSON(http.StatusForbidden, saveErr.Error())
            return
        }
        tokens := map[string]string{
            "access_token":  ts.AccessToken,
            "refresh_token": ts.RefreshToken,
        }
        c.SetCookie("access_token" ,  tokens["access_token"], HttpCookieMaxAge, HttpCookiePath, HttpCookieDomain, HttpCookieSameSite, HttpCookieSecure, HttpCookieHttpOnly)
        c.SetCookie("refresh_token", tokens["refresh_token"], HttpCookieMaxAge, HttpCookiePath, HttpCookieDomain, HttpCookieSameSite, HttpCookieSecure, HttpCookieHttpOnly)

        c.JSON(http.StatusCreated, tokens)
    } else {
        c.JSON(http.StatusUnauthorized, "refresh expired")
    }
}

func ExtractToken(r *http.Request) string {
    var cookie,err = r.Cookie("access_token")
    if err == nil {
        return cookie.Value
        // Not Supossing "Bearer__TOKEN ?? Be carefull 3:)
    } else {
        log.Println("cookierror", err.Error())
        return ""
    }
}

func ExtractRefreshToken(r *http.Request) string {
    var cookie,err = r.Cookie("refresh_token")
    if err == nil {
        return cookie.Value
        // Not Supossing "Bearer__TOKEN ?? Be carefull 3:)
    } else {
        log.Println("cookierror", err.Error())
        return ""
    }
}

func VerifyToken(r *http.Request) (*jwt.Token, error) {
    tokenString := ExtractToken(r)
    token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
        //REALLY AS "SigningMethodHMAC" ???
        if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
            return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
        }
        return []byte(os.Getenv("REDIS_ACCESS_SECRET")), nil
    })
  
    if err != nil {
        return nil, err
    }
    return token, nil
}

func VerifyRefreshToken(r *http.Request) (*jwt.Token, error) {
    tokenString := ExtractRefreshToken(r)
    token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
        //REALLY AS "SigningMethodHMAC" ???
        if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
            return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
        }
        return []byte(os.Getenv("REDIS_REFRESH_SECRET")), nil
    })
  
    if err != nil {
        return nil, err
    }
    return token, nil
}

func TokenValid(r *http.Request) error {
    token, err := VerifyToken(r)
    if err != nil {
        return err
    }
    if _, ok := token.Claims.(jwt.Claims); !ok && !token.Valid {
        return err
    }
    return nil
}

func ExtractTokensMetadata(r *http.Request) (*AccessDetails, error) {

    var theDetails AccessDetails

    token, err := VerifyToken(r)
    if err != nil {
        return nil, err
    }
    claims, ok := token.Claims.(jwt.MapClaims)
    if ok && token.Valid {
        accessUuid, ok := claims["access_uuid"].(string)
        if !ok {
            return nil, err
        }
        userId, err := strconv.ParseUint(fmt.Sprintf("%.f", claims["user_id"]), 10, 64)
        if err != nil {
            return nil, err
        }

        theDetails.AccessUuid = accessUuid
        theDetails.UserId = userId
    }

    token, err = VerifyRefreshToken(r)
    if err != nil {
        log.Println("end ??")
        return nil, err
    }
    claims, ok = token.Claims.(jwt.MapClaims)
    if ok && token.Valid {
        refreshUuid, ok := claims["refresh_uuid"].(string)
        if !ok {
            log.Println("end ?? 12")
            return nil, err
        }

        theDetails.RefreshUuid = refreshUuid
    }

    return &theDetails, nil
}
