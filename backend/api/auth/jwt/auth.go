package authjwt

import (
    "strconv"
    "time"
    
    rds "thebtrnmnt/backend/api/dbs/redisservice/dbservice"
)

func FetchAuth(authD *AccessDetails) (uint64, error) {
    userid, err := rds.RedisClient.Get(authD.AccessUuid).Result()
    if err != nil {
        return 0, err
    }
    userID, _ := strconv.ParseUint(userid, 10, 64)
    return userID, nil
}

func CreateAuth(userid uint64, td *TokenDetails) error {
    at := time.Unix(td.AtExpires, 0) //converting Unix to UTC(to Time object)
    rt := time.Unix(td.RtExpires, 0)
    now := time.Now()
 
    errAccess := rds.RedisClient.Set(td.AccessUuid, strconv.Itoa(int(userid)), at.Sub(now)).Err()
    if errAccess != nil {
        return errAccess
    }
    errRefresh := rds.RedisClient.Set(td.RefreshUuid, strconv.Itoa(int(userid)), rt.Sub(now)).Err()
    if errRefresh != nil {
        return errRefresh
    }
    return nil
}

func DeleteAuth(givenUuid string) (int64,error) {
    deleted, err := rds.RedisClient.Del(givenUuid).Result()
    if err != nil {
        return 0, err
    }
    return deleted, nil
}
