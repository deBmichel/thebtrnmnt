package authjwt
 
import (
	"github.com/gin-gonic/gin"
)

const swsgroup = "/bllrttrnmnt/auth"
const cmmnerrrws = "/bllrttrnmnt/auth fail auth, contact development bllrttrnmnt team ...."

func WSAuth (route *gin.Engine) {
	authRouter := route.Group(swsgroup)   

	authRouter.POST("/login", AuthLogin)
	authRouter.POST("/logout", TokenAuthMiddleware(), AuthLogout)
	authRouter.POST("/logcreate", AuthLogcreate)
	authRouter.POST("/tokenrefresh", TokenAuthMiddleware(), RefreshTokens)
}
