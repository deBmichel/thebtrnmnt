package authjwt
 
import (
	  "log"
	  "net/http"

	"github.com/gin-gonic/gin"

	users "thebtrnmnt/backend/api/auth/users"
)

func TokenAuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := TokenValid(c.Request)
		if err != nil {
			log.Println("Fail in TokenAuthMiddleware() 1", err.Error())
			c.JSON(http.StatusUnauthorized, "unauthorized")
			c.Abort()
			return
		}

		tokenAuth, err := ExtractTokensMetadata(c.Request)
		if err != nil {
			log.Println("Fail in TokenAuthMiddleware() 2", err.Error())
			c.JSON(http.StatusUnauthorized, "unauthorized")
			c.Abort()
			return
		}

		userId, err := FetchAuth(tokenAuth)
		if err != nil || userId <= 0 {
			log.Println("Fail in TokenAuthMiddleware() 3", err.Error())
			c.JSON(http.StatusUnauthorized, "unauthorized")
			c.Abort()
			return
		}

		c.Next()
	}
}

var (
	HttpCookiePath = "/"
	HttpCookieDomain = ".localhost"
	HttpCookieMaxAge = 604800
	HttpCookieMaxBreak = -2
	HttpCookieSameSite = http.SameSiteLaxMode
	HttpCookieSecure = false
	HttpCookieHttpOnly = false
)

func AuthLogin(c *gin.Context) {
	var u users.UserAuth

	if err := c.ShouldBindJSON(&u); err != nil {
  	  	log.Println("Fail in Login 1", err.Error())
		c.JSON(http.StatusUnprocessableEntity, gin.H{
  			"Answer": "Problems with data: you are sending bad data",
  		})
		return
  	}

  	validate, IdUser, err := users.VerifyLoginDataUser(u.Identifier, u.Password)

	if err != nil {
	  log.Println("Fail in Login 2", err.Error())
	  c.JSON(http.StatusUnprocessableEntity, cmmnerrrws)
	  return
	}

  	if validate {
		tokenDetails, err := CreateTokens(IdUser)
	  	if err != nil {
	  		log.Println("Error generating token on login service ??? be Carefull Cowboy ....", err.Error())
            c.JSON(http.StatusUnprocessableEntity, cmmnerrrws)
			return
	  	} else {
	  	    saveErr := CreateAuth(IdUser, tokenDetails)
			if saveErr != nil {
                log.Println("I Erron on CreateAuth(IdUser,", saveErr.Error())
                c.JSON(http.StatusUnprocessableEntity, cmmnerrrws)
				return
			} else {
				tokens := map[string]string{
					"access_token":  tokenDetails.AccessToken,
					"refresh_token": tokenDetails.RefreshToken,
				}

				c.SetCookie("access_token" ,  tokens["access_token"], HttpCookieMaxAge, HttpCookiePath, HttpCookieDomain, HttpCookieSameSite, HttpCookieSecure, HttpCookieHttpOnly)
				c.SetCookie("refresh_token", tokens["refresh_token"], HttpCookieMaxAge, HttpCookiePath, HttpCookieDomain, HttpCookieSameSite, HttpCookieSecure, HttpCookieHttpOnly)

				c.JSON(http.StatusOK, gin.H{
				  "Answer": "Correct login ...",
				  //"tokens": tokens,
				})
		  	   return
			}
	  	}
  	} else {
  		if err != nil {
  			log.Println("Error validating for token on login service ??? be Carefull Cowboy ....", err.Error())
		    c.JSON(http.StatusUnprocessableEntity, cmmnerrrws)
  			return
  		} else {

  			c.JSON(http.StatusUnauthorized, gin.H{
  				"Answer": "Please provide valid login details",
  			})
  			return
  			
  		}
  	}
}

func AuthLogout(c *gin.Context) {
	au, err := ExtractTokensMetadata(c.Request)
	if err != nil {
		c.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}
	
	deleted, delErr := DeleteAuth(au.AccessUuid)
	if delErr != nil || deleted == 0 { //if any goes wrong
		c.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}

	deleted, delErr = DeleteAuth(au.RefreshUuid)
	if delErr != nil || deleted == 0 { //if any goes wrong
		c.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}

	c.SetCookie("access_token" , "access_token" , HttpCookieMaxBreak, HttpCookiePath, HttpCookieDomain, HttpCookieSameSite, HttpCookieSecure, HttpCookieHttpOnly)
	c.SetCookie("refresh_token", "refresh_token", HttpCookieMaxBreak, HttpCookiePath, HttpCookieDomain, HttpCookieSameSite, HttpCookieSecure, HttpCookieHttpOnly)
	c.JSON(http.StatusOK, "Successfully logged out")
}

func AuthLogcreate(c *gin.Context) {
	log.Println("Creating users:...")
	var u users.User

	if err := c.ShouldBindJSON(&u); err != nil {
		log.Println(err.Error())
		c.JSON(http.StatusUnprocessableEntity, "Really an Invalid json was provided")
		return
	}

	users.CreateUser(&u)
}
