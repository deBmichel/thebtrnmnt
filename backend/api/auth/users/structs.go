package authusers

type User struct {
	ID     uint64 `gorm:"primary_key;auto_increment" json:"id"`
	Alias  string `gorm:"size:100;not null;" json:"Alias" binding:"required"`
	Psswr  string `gorm:"size:100;not null;" json:"Psswr" binding:"required"`	 	 
}

type UserAuth struct {
	ID		    uint64    `json:"id"`
	Identifier  string    `json:"identifier" binding:"required"`
	Password    string    `json:"password" binding:"required"`
}
