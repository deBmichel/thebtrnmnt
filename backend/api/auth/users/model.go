package authusers

import (
    "log"
    "errors"

    DBS "thebtrnmnt/backend/api/dbs/postgresservice/dbservice"
)

var (
    ErrUserExists = errors.New("User Exist in the db, contact Development...")
    ErrUserNotExists = errors.New("User DOES NOT Exist in the db, update or contact Development...")
)

func CreateUser(usr *User) (*User, error) {
    exist, err := UserExist(usr.Alias)
    if err != nil {
        return nil, err
    }

    if exist {
        return nil, ErrUserExists
    }

    err = DBS.PostresClient.Exec(`
        INSERT INTO apiusers (alias, password) VALUES (?, ?)`, 
            usr.Alias, 
            usr.Psswr,
        ).Error
    if err != nil {
        return nil, err
    }

    return usr, nil
}

func UserExist(alias string) (bool, error) {
    usr, err := GetDataUser(alias)
    if err != nil {
        return false, err
    } else {
        return (usr.Alias == alias), nil
    }
}

func VerifyLoginDataUser (alias, password string) (flag bool, uint uint64, err error) {
    usr, err := GetDataUser(alias)
    if err != nil {
        log.Println("Error on VerifyLoginDataUser", err.Error())
        return false, uint, err
    } else {
        // Direct : implementation using encription on password is a great idea
        return (usr.Alias == alias && usr.Psswr == password), usr.ID, nil
    }    
}

func GetDataUser(alias string) (*User, error) {
    var usr User

    err := DBS.PostresClient.Raw(`
        SELECT id, alias, password AS Psswr FROM ApiUsers 
        WHERE alias = ?
    `, alias).Scan(&usr).Error
    if err != nil {
        if err.Error() == "record not found" {
            log.Println("Failing in VerifyLoginDataUser executing unique select ... possible cause : User Does not exist in the db")
            log.Println("Fails with data user:", alias)
            err = ErrUserNotExists
        }
        log.Println("Error diferent to record not found", err.Error())
        return nil, err
    } else {
        return &usr, nil
    }
}


