package main

import (
	"log"
	"net/http"
	"time"

	_ "github.com/joho/godotenv/autoload"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	wsAuth "thebtrnmnt/backend/api/auth/jwt"
	wsCnddtsrgstr "thebtrnmnt/backend/api/cnddtsrgstr"
)

const AllowedOrigin = "http://localhost:4000"

func OptionsAuthMiddleware() gin.HandlerFunc {
    return func(c *gin.Context) {
    	c.Writer.Header().Add("Access-Control-Allow-Origin", AllowedOrigin)
	    if c.Request.Method != "OPTIONS" {
            c.Next()
        } else {
            log.Println("iS OPTIONS ??")
	        c.JSON(http.StatusOK, gin.H{
				"Access-Control-Allow-Origin": "http://localhost:4000",
			})
        }
    }
}

func main () {
	mainRouter := gin.Default()
	mainRouter.Use(OptionsAuthMiddleware())
	mainRouter.Use(cors.New(cors.Config{
		AllowOrigins:     []string{AllowedOrigin}, 
		AllowMethods:     []string{"POST", "GET", "OPTIONS", "DELETE", "PATCH"},
		ExposeHeaders:    []string{"Content-Length", "Content-Type", "Access-Control-Allow-Methods"},
		AllowHeaders:     []string{"Content-Type", "Content-Length", "Accept-Encoding", "Authorization", "Accept", "Origin", "Access-Control-Allow-Origin", "User-Agent", "Referrer", "Host"},
		AllowCredentials: true,
		/*AllowOriginFunc: func(origin string) bool {
			return origin == "https://ALTERNATIVE"
		},*/
		MaxAge: 12 * time.Hour,
	}))

	wsAuth.WSAuth(mainRouter)
	wsCnddtsrgstr.WSCnddtsrgstr(mainRouter)
	
	mainRouter.Run()
}