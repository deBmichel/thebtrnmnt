package dbservice

import (
	"os"
	"fmt"
	"log"

	"gorm.io/driver/postgres"
  	"gorm.io/gorm"
)

var PostresClient *gorm.DB

func init(){
	var (
		DbUser = os.Getenv("POSTGRES_DB_USER")
		DbPassword = os.Getenv("POSTGRES_DB_PASSWORD")
		DbHost = os.Getenv("POSTGRES_DB_HOST")
		DbName = os.Getenv("POSTGRES_DB_NAME")
		DbPort = os.Getenv("POSTGRES_DB_PORT")
		err error
	)

	DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", DbHost, DbPort, DbUser, DbName, DbPassword)
	
	PostresClient, err = gorm.Open(postgres.Open(DBURL), &gorm.Config{})	
	if err != nil {
		log.Println("Postgres failed in init")
		panic(err)
	}

	log.Println("Postgres Really has Init; \n Postgres ready to use in minor services")
}

