create database bllrttrnmnt;

\c bllrttrnmnt;

CREATE OR REPLACE FUNCTION trigger_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_date = NOW();
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

create table Candidates(
	ID            BIGSERIAL PRIMARY KEY,
	NID           BIGINT NOT NULL,
	Name          VARCHAR(100) NOT NULL,
	Nickname      VARCHAR(100) NOT NULL DEFAULT 'NotNickName',
	Payment       BOOLEAN DEFAULT false,
	creation_date TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_date  TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TRIGGER set_timestamp
BEFORE UPDATE ON Candidates
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();

create table ApiUsers(
	ID            BIGSERIAL PRIMARY KEY,
	Alias         VARCHAR(100) NOT NULL,
	Password      VARCHAR(100) NOT NULL,
	creation_date TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_date  TIMESTAMPTZ NOT NULL DEFAULT NOW()	
);

CREATE TRIGGER set_timestamp
BEFORE UPDATE ON ApiUsers
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();


