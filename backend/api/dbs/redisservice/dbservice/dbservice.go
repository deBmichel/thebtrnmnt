package dbservice

import (
	"log"
    "os"
    
	"github.com/go-redis/redis"
)

var RedisClient *redis.Client

// Using init as redis tester. If ping to redis fails the service should fails starting.
func init(){
    dsn, passw := os.Getenv("REDIS_DSN"), os.Getenv("REDIS_PASS")
    if len(dsn) == 0 {
        dsn = os.Getenv("REDIS_DSN_DEFAULT")
    }
    RedisClient = redis.NewClient(&redis.Options{
        Addr:     dsn,    //redis identifier with port
        Password: passw,  //redis password 
    })
    
    _, err := RedisClient.Ping().Result()
    if err != nil {
        log.Println("Redis failed in init")
        panic(err)
    }

    log.Println("Redis Really has Init; \n Redis ready to use in minor services")
}

