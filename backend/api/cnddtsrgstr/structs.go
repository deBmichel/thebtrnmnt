package cnddtsrgstr

type Candidate struct {
	NID       uint64 `gorm:"size:100;not null;" json:"nid" binding:"required"`
	Name      string `gorm:"size:100;not null;" json:"Name" binding:"required"`
	Nickname  string `gorm:"size:100;default:'NotNickName';" json:"Nickname" binding:"required"`
	Payment   bool   `gorm:"size:5  ;not null;" json:"Payment" binding:"required"`	
}
