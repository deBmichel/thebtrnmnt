package cnddtsrgstr

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func PostCnddtsrgstr(c *gin.Context) {
	var cnddt Candidate

	if err := c.ShouldBindJSON(&cnddt); err != nil {
  	  	log.Println("Invalid json ? in PostCnddtsrgstr 1", err.Error())
		c.JSON(http.StatusUnprocessableEntity, "Really an Invalid json was provided")
		return
  	}

  	exist, candidate, err := CandidateExists(cnddt.NID)

  	if err != nil {
  		log.Println("Error on PostCnddtsrgstr in CandidateExists(cnddt.NID)", err.Error)
  		c.JSON(http.StatusUnprocessableEntity, cmmnerrrws)
  		return
  	}

  	if !exist {
  		err := CreateCandidate(&cnddt)
  		if err != nil {
  			log.Println("Error on PostCnddtsrgstr in CreateCandidate(&c)", err.Error())
  			c.JSON(http.StatusUnprocessableEntity, cmmnerrrws)
  			return
  		}

  		c.JSON(http.StatusOK, gin.H{
			"Answer": "Candidate was created",
			"Candidate": c,
		})
		return
  	} else {
  		c.JSON(http.StatusOK, gin.H{
			"Answer": "Candidate was NOT CREATED, THE NID was registered",
			"Candidate Data Asociated to nid": candidate,
		})
		return
  	}
}

func GetCnddtsrgstr (c *gin.Context) {
	candidates, err := GetCandidates()
	if err != nil {
		log.Println("Error on GetCnddtsrgstr in GetCandidates()", err.Error())
		c.JSON(http.StatusUnprocessableEntity, cmmnerrrws)
  		return
	} else {
		c.JSON(http.StatusOK, gin.H{
			"Answer": "List of candidates was generated",
			"Candidates": candidates,
		})
		return	
	}
}