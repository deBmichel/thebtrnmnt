package cnddtsrgstr

import (
	"github.com/gin-gonic/gin"

	auth "thebtrnmnt/backend/api/auth/jwt"
)

const swsgroup = "/bllrttrnmnt/cnddtsrgstr"
const cmmnerrrws = "/bllrttrnmnt/cnddtsrgstr fail auth, contact development bllrttrnmnt team ...."

func WSCnddtsrgstr (route *gin.Engine) {
	cnddtsrgstrRouter := route.Group(swsgroup)   

	cnddtsrgstrRouter.POST("/", auth.TokenAuthMiddleware(), PostCnddtsrgstr)
	cnddtsrgstrRouter.GET("/",  auth.TokenAuthMiddleware(), GetCnddtsrgstr )
}
