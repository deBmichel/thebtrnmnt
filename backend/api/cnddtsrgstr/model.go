package cnddtsrgstr

import (
	"log"
	"errors"

	DBS "thebtrnmnt/backend/api/dbs/postgresservice/dbservice"
)

var(
    ErrCandidateExists = errors.New("Candidate Exist in the db, contact Development...")
    ErrCandidateNotExists = errors.New("Candidate DOES NOT Exist in the db, update or contact Development...")
)

func CreateCandidate(c * Candidate) error {
	return DBS.PostresClient.Exec(`
    		INSERT INTO candidate (NID, Name, Nickname, Payment) VALUES (?, ?, ?, ?)`, 
            c.NID, c.Name, c.Nickname, c.Payment,
    ).Error
}

func CandidateExists(nid uint64) (bool, *Candidate, error){
	candidate, err := GetDataCandidate(nid)
	if err != nil {
		if err == ErrCandidateNotExists {
			return false, nil, nil
		} else {
			return false, nil, err
		}
	}

	return candidate.NID == nid, candidate, nil
}

func GetDataCandidate(nid uint64) (*Candidate, error) {
    var candidate Candidate

    err := DBS.PostresClient.Raw(`
        SELECT NID,Name, Nickname, Payment FROM candidates WHERE NID = ?
    `, nid).Scan(&candidate).Error
    if err != nil {
        if err.Error() == "record not found" {
            log.Println("Failing in GetDataCandidate executing unique select ... possible cause : Candidate Does not exist in the db")
            log.Println("Fails with data candidate:", nid)
            err = ErrCandidateNotExists
        }
        return nil, err
    } else {
        return &candidate, nil
    }
}

func GetCandidates() (ret [] Candidate, err error) { 
	err = DBS.PostresClient.Raw(`
        SELECT NID,Name, Nickname, Payment FROM candidates
    `).Scan(&ret).Error
    return ret, err
}