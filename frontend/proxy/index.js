const express = require('express');
const morgan = require("morgan");
const { createProxyMiddleware } = require('http-proxy-middleware');

// Create Express Server
const app = express();

// Configuration
const PORT = 4000;
const HOST = "localhost";
const API_SERVICE_URL = "http://localhost:8080";

// Logging
app.use(morgan('dev'));

// Info GET endpoint
app.get('/info', (req, res, next) => {
    res.send('This is a proxy service which proxies to JSONPlaceholder API.');
});

// Authorization
app.use('', (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "http://localhost:3000"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var cookie;
function relayRequestHeaders(proxyReq, req) {
  if (cookie) {
    proxyReq.setHeader('cookie', cookie);
  }
};

// Proxy endpoints
app.use('/bllrttrnmnt/cnddtsrgstr', createProxyMiddleware({
    target: API_SERVICE_URL+"/bllrttrnmnt/cnddtsrgstr",
    changeOrigin: true,
    onProxyReq: relayRequestHeaders,
    pathRewrite: {
        [`/bllrttrnmnt/cnddtsrgstr`]: '',
    },
}));

 /*proxyReqOptDecorator: function(proxyReqOpts, srcReq) {
      proxyReqOpts.headers['cookie'] = 'cookie-string';
      return proxyReqOpts;
    }*/

app.use('/bllrttrnmnt/auth/login', createProxyMiddleware({
    target: API_SERVICE_URL+"/bllrttrnmnt/auth/login",
    changeOrigin: true,
    pathRewrite: {
        [`/bllrttrnmnt/auth/login`]: '',
    },
}));




// Start Proxy
app.listen(PORT, HOST, () => {
    console.log(`Starting Proxy at ${HOST}:${PORT}`);
});
