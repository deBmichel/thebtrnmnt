import {
	VALIDLOGGINANSWER, 
	ERRORUSERDATALOGGIN, 
	ERRORSYSTEM,
	ERRORAPILOGGINFAILSBYBADDATA
} from './constants.js';

const ValidateLoggin = (jsonLogginAnswer) => {
	if (jsonLogginAnswer['Answer'] === VALIDLOGGINANSWER) {
		return [true, VALIDLOGGINANSWER];
	} else {
		if (jsonLogginAnswer['Answer'] === ERRORUSERDATALOGGIN) {
			return [false, ERRORUSERDATALOGGIN];
		} 

		if (jsonLogginAnswer['Answer'] === ERRORAPILOGGINFAILSBYBADDATA) {
			return [false, ERRORUSERDATALOGGIN];	
		}	
	}
	console.log("Really Found a diferent error", jsonLogginAnswer);
	return [false, ERRORSYSTEM];
}

export {ValidateLoggin}