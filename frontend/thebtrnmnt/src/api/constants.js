// Default constant to manage error system. 
const ERRORSYSTEM = "ERRORSYSTEM";

// ----------------------------- Begin CONSTS Related with Api and loggin functions.
	const VALIDLOGGINANSWER = "Correct login ...";
	const ERRORUSERDATALOGGIN = "Please provide valid login details";
	const ERRORAPILOGGINFAILSBYBADDATA = "Problems with data: you are sending bad data";
// End ------------------------------- CONSTS Related with Api and loggin functions.

export {
	ERRORSYSTEM,

	VALIDLOGGINANSWER, 
	ERRORUSERDATALOGGIN, 
	ERRORAPILOGGINFAILSBYBADDATA

}