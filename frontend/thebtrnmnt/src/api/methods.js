var endpointapiurl = "";
var endpointfront  = "http://localhost:3000";

//Begin : Section generic to operation methods to PATCH, UPDATE, GET, DELETE

//--------------------------------------Begin Post Section:

const baseFetchPostHeaders = () => {
    return {
        method: 'POST',
        headers: {
            'Access-Control-Allow-Origin': endpointfront
        }
    };
};

const DoFetchPost = async (endpointUrl, data, needauth, ansmanager) => {
    var fetchHeaders = baseFetchPostHeaders();
    if (needauth) { fetchHeaders['credentials'] = 'include'; }

    var url = endpointapiurl + endpointUrl;
    fetchHeaders['body'] = data;
    
    let answerpost = undefined;

    const postPromise = await fetch(url, fetchHeaders);
    try {
        answerpost = await postPromise.json();
        console.log("Arrived", answerpost);
    } catch(e) {
        answerpost = e;
        console.log('error:', e, e.message);
    }
        
    return answerpost;
}

//End ----------------------------------Post Section

//--------------------------------------Begin Get Section:

const DoFetchGet = (endpointUrl, data, needauth, ansmanager) => {
    console.log("try arrive here .......");
    var url = endpointapiurl + endpointUrl;
    fetch(url, 
        {   
            method: "GET",
            headers: {
                'Access-Control-Allow-Origin': 'http://localhost:3000'
            },
            credentials: 'include'
        }
    ).then(function(response) {
        if (!response.ok) {
            console.log("problem", response.statusText);
            throw new Error(response.statusText);
        }
        return response.json();
    }).then(function (data) {
        ansmanager(data);
    }).catch(function(error) {
        return error;
    })
}








/*const baseFetchGetHeaders = () => {
    return {
        method: "GET",
        headers: {
            'Access-Control-Allow-Origin': endpointfront
        }
    };
};

export const DoFetchGet = (endpointUrl, data, needauth, ansmanager) => {
    console.log("try arrive here .......");
    let fetchHeaders = baseFetchGetHeaders();
     if (needauth) {
        fetchHeaders['credentials'] = 'include';
    }
    var url = endpointapiurl + endpointUrl;
    fetch(url, fetchHeaders).then(function(response) {
        if (!response.ok) {
            console.log("problem", response.statusText);
            throw new Error(response.statusText);
        }
        return response.json();
    }).then(function (data) {
        ansmanager(data);
    }).catch(function(error) {
        return error;
    })
}*/

//End ----------------------------------Get Section


//End : Section generic to operation methods to PATCH, UPDATE, GET, DELETE


export {DoFetchPost, DoFetchGet}