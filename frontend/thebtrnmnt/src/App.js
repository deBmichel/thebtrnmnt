import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import * as Home from './components/home/home.js'
import * as Logn from './components/loggin/loggin.js'

export default function App() {
  return (
    <Router>
      <div>
        <Home.HomeNav/>
        <Switch>
          <Route path="/about">
            <Home.HomeAbout />
          </Route>
          <Route path="/users">
            <Home.HomeUsers />
          </Route>
          <Route path="/loggin">
            <Logn.Loggin />
          </Route>
          <Route path="/">
            <Home.HomeInit />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

