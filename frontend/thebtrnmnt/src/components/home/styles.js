export const homeComponentUl = {
    'listStyle':'none',
    'margin':'0',
    'padding':'0',
    'textAlign':'center'
}

export const homeComponentLi = {
    'display': 'inline',
    'margin': '1px 0 0 0',
    'padding': '0',
    'listStyleType': 'none'
}

export const homeComponent = {
    'margin': 'auto',
	'width': '100%',
	'padding': '10px',
	'textAlign':'center'
}

export const imageStyle = {
	'display': 'block',
  	'marginLeft': 'auto',
  	'marginRight': 'auto',
  	'width': '10%',
  	'height': '10%',
  	'border': '6px solid green',
  	'borderRadius': '25px'
}