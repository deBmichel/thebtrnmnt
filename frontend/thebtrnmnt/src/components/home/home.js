import React from "react";
import {Link} from "react-router-dom";
import Button from '@material-ui/core/Button';

import Logo from './rsrcs/BllrtTrnlog.png';
import * as styles from './styles.js';

export const HomeNav = () => {
	return <>
		<nav style = {styles.homeComponent}>
			<img style={styles.imageStyle} src={Logo} alt="logo" />
			<br />
			Billiart Tournament App<br />
			<br />
			<ul style = {styles.homeComponentUl}>
				<li style = {styles.homeComponentLi}>
              		<Link to="/">
                		<Button variant="contained" color="primary">
                  			Home
                		</Button>
              		</Link>
				</li>
				<li style = {styles.homeComponentLi}>
					<Link to="/about">
                		<Button variant="contained" color="primary">
                  			About
                		</Button>
              		</Link>
				</li>
				<li style = {styles.homeComponentLi}>
					<Link to="/users">
						<Button variant="contained" color="primary">
                  			Users
                		</Button>
					</Link>
				</li>
				<li style = {styles.homeComponentLi}>
					<Link to="/loggin">
						<Button variant="contained" color="primary">
                  			Loggin
                		</Button>
					</Link>
				</li>		
			</ul>
        </nav>
    </>;
}


export const HomeAbout = () => {
	return <>
		<h1>
			About Billiart app: 
		</h1>
		Billiart app is an app created to play 
		billiart tournaments, <br />the name of the 
		mind of the idea is Jhon Jairo, the name <br />
		of the developer is Michel.
	</>;
}


export const HomeUsers = () => {
	return <>
		<h1>
			Welcome to the Billiart app User:
		</h1>
		Go player, go to play a billiart tournament.
	</>;
}

export const HomeInit = () => {
	return <>
		<h1>Welcome to the Billiart app:</h1>
		It is a simple app to execute
		billiart tournaments in my 
		town.
	</>;
}
