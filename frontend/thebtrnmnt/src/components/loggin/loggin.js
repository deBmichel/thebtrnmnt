import React, {useState} from "react";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Image from './rsrcs/billiard.gif';


import {ExecuteLoggin} from './loggincontroller.js';

export const Loggin = () => {
	function sleep(seconds) {
		let ms = seconds * 1000;
		return new Promise(resolve => setTimeout(resolve, ms));
	}

	const [user, setUser] = useState('');
	const [pass, setPass] = useState('');
	const DoLoggin = () => {
		// AS PROMISES ARE THE HELL, ASSUME YOU ARE WORKING WITH PROMISES :) 
		let logginstate = ExecuteLoggin(user, pass);
		// INfernal Promise in JS : they are crazy
		logginstate.then(async function(state){
			console.log("State", state);
			setTitle(state[0]);
			setMessage(state[1]);
			handleClickOpen();
			if (state[1] === CorrectLogin) {
				setOpenBtn(false);
				await sleep(9);
				handleClose();
			} else {
				setOpenBtn(true);
				await sleep(5);
				handleClose();
			}			
		});
	}
	
	const [open, setOpen] = React.useState(false);
	const [openbtn, setOpenBtn] = React.useState(true);
	const [title, setTitle] = React.useState(false);
	const [message, setMessage] = React.useState(false);
	const theme = useTheme();
	const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
	const handleClickOpen = () => {
    	setOpen(true);
  	};
  	const handleClose = () => {
    	setOpen(false);
  	};

  	return (
    	<div>
    		<form>
				<h2>User</h2>  
				<input 
					id="user" 
					type="text" 
					onChange={event => setUser(event.target.value)}
				/>
				<h2>Password</h2>
				<input 
					id="passw" 
					type="password"
					autoComplete="current-password"
					onChange={event => setPass(event.target.value)}/
				>
				<br /><br />
	    		<Button variant="outlined" color="primary" onClick={DoLoggin}>
	        		Loggin
	    		</Button>
	    	</form>

	    	<Dialog
	        	fullScreen={fullScreen} open={open}
	        	onClose={handleClose}  	aria-labelledby="responsive-dialog-title"
	    	>
        		<DialogTitle id="responsive-dialog-title">
        			{title}
        		</DialogTitle>
		        <DialogContent>
		        	{message}
		        </DialogContent>
        		<DialogActions>
        			{openbtn && 
        				<Button onClick={handleClose} color="primary" autoFocus>
	            			Agree
	          			</Button>
	          		}
        		</DialogActions>
      		</Dialog>
    	</div>
	);
}


const ErrorData = () => {
	return (
		<>
			<DialogContentText>
			    The data you entered to log in is wrong. <br />
			    Please review or contact development.
			</DialogContentText>
		</>
	);
}

const ErrorSystem = () => {
	return (
		<>
			<DialogContentText>
			    Really we are having problems with the system<br />
			    Please contact our development team.
			</DialogContentText>
		</>
	);
}

const CorrectLogin = () => {
	return (
		<>
			<DialogContentText>
			    Give a little moment.
			</DialogContentText>
			<img src={Image} alt="logo" />
		</>
	);	
}

export {ErrorData, ErrorSystem, CorrectLogin}