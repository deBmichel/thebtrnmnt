import {DoFetchPost} from '../../api/methods.js';
import {ValidateLoggin} from '../../api/corelogic.js';
import {VALIDLOGGINANSWER, ERRORUSERDATALOGGIN, ERRORSYSTEM} from '../../api/constants.js';
import {ErrorData, ErrorSystem, CorrectLogin} from './loggin.js';

const LogginPosibleStatus = [
    'ToDo', 
    ERRORSYSTEM, 
    ERRORUSERDATALOGGIN, 
    VALIDLOGGINANSWER
];

const LoadJson = (user, pass) => {
    var payload = {
        identifier: user,
        password: pass
    };

    return JSON.stringify( payload );
}

const ExecuteLoggin = (user, pass) => {
    let endpointUrl = "/bllrttrnmnt/auth/login";
    let data = LoadJson(user, pass);
    var answer = DoFetchPost(endpointUrl, data, false);
    // As promises are the hell, simple: RETURN THE PROMISE :) 
    return answer.then(function(answerpost){
        var validation = ValidateLoggin(answerpost);
        if (validation[0]) {
            return [
                "Correct \n Charging User Interface", 
                CorrectLogin
            ];
        } 

        if (validation[1] === ERRORUSERDATALOGGIN) {
            return [
                "The data you entered to log in is wrong?", 
                ErrorData
            ];
        }

        if (validation[1] === ERRORSYSTEM) {
            return [
                "We are having problems:", 
                ErrorSystem
            ];
        }
    }); 
}





export {ExecuteLoggin, LogginPosibleStatus}






